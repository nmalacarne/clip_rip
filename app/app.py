import uuid
import os

from dotenv import load_dotenv
from flask import Flask, render_template, request, send_file, flash
from wtforms import Form, validators
from wtforms.fields.html5 import URLField, IntegerField
from pytube import YouTube
from pytube.exceptions import RegexMatchError, VideoUnavailable
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
from moviepy.audio.io.AudioFileClip import AudioFileClip

load_dotenv()

app = Flask(__name__)
app.secret_key = os.getenv('SECRET_KEY', uuid.uuid4().hex)


class RipClipForm(Form):
    url = URLField(validators=[
        validators.required(),
        validators.url(),
    ])

    start_time = IntegerField(validators=[
        validators.optional(),
        validators.number_range(min=0),
    ])

    end_time = IntegerField(validators=[
        validators.required(),
        validators.number_range(min=1, max=20),
    ])


def download(url, file_dir):
    yt = YouTube(url)

    return yt.streams.filter(
        file_extension='mp4',
        only_audio=True
    ).first().download(
        output_path=file_dir,
    )


def clip(file_path, start_time, end_time):
    clip_name = file_path.replace('.mp4', '.clip.mp4')

    ffmpeg_extract_subclip(
        file_path,
        start_time,
        start_time + end_time,
        targetname=clip_name
    )

    clip = AudioFileClip(clip_name)
    clip.write_audiofile(f"{file_path[:-4]}.mp3")
    clip.close()

    return f"{file_path[:-4]}.mp3"


@app.route('/', methods=['GET', 'POST'])
def index():
    form = RipClipForm(request.form)

    if request.method == 'POST' and form.validate():
        try:
            file_dir = f"/tmp/cliprip/{uuid.uuid4().hex}"

            file_path = download(form.url.data, file_dir)

            file_path = clip(
                file_path,
                form.start_time.data,
                form.end_time.data
            )

            return send_file(file_path, as_attachment=True)
        except RegexMatchError:
            flash('Could not find URL')
        except VideoUnavailable:
            flash('Video unavailable')

    return render_template('index.html', form=form)
