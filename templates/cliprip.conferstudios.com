server {
	listen 443 ssl;	
	server_name cliprip.conferstudios.com;
	if ($host != "cliprip.conferstudios.com") {
		return 404;
	}
	ssl_certificate /etc/letsencrypt/live/cliprip.conferstudios.com/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/cliprip.conferstudios.com/privkey.pem;
	ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
	include /etc/letsencrypt/options-ssl-nginx.conf;
	location / {
		include uwsgi_params;
		uwsgi_pass unix:/var/www/cliprip/app.sock;
	}
}

