# Clip Rip
## Rip audio clips from YouTube

### Installation

```bash
git clone git@gitlab.com:nmalacarne/clip_rip.git
cd clip_rip
python3 -m venv venv
source venv/bin/activate
pip install -r app/requirements.txt
FLASK_APP=app/app.py FLASK_ENV=development flask run
```

### VM Setup

Install Vagrant and `vagrant up`

### Notes

Writes to `/tmp` so make sure you have that...
